load('output/beamformer/source_data_D2_8-12_V5.mat');
load('T36.mat')
addpath(genpath('HMM-MAR-master 20180107'))
addpath('fieldtrip-20210914')
ft_defaults
%% Make directories
%% Load Beta coeficients and create output directories
mkdir('output/figures/agonist');
mkdir('output/figures/placebo');
mkdir('output/figures/antagonist');
mkdir('output/figures/agonist/subjects/');
mkdir('output/figures/placebo/subjects');
mkdir('output/figures/antagonist/subjects');
mkdir('output/figures/pngs/agonist');
mkdir('output/figures/pngs/placebo');
mkdir('output/figures/pngs/antagonist');
mkdir('output/figures/bar');
mkdir('output/figures/bar_thresholded');
mkdir('output/figures/surface');
mkdir('output/areas/agonist');
mkdir('output/areas/antagonist');
mkdir('output/areas/placebo');
mkdir('output/areas/all_subjects');
mkdir('output/figures/pngs/all_subjects');
mkdir('output/figures/HsMM');
%% Load Headmodel, Sourcemodel and Atlas. Also Sourceint, ready to plot
files = dir('source_maps_models/*.mat');
for i = 1:size(files,1)
    load([files(i).folder '/' files(i).name])
    disp(['Loaded: ' files(i).name])
end

%% define the variables to use and parameters
n_states = 7;
n_sources = size(source_data,1);
fs = 500;
fs_new = 250;
%% Conditions matrix
agonist     = [2 6 9 11 13 18 21 22 25 30 31 34]; % Cabergoline
placebo     = [1 4 8 12 15 16 20 24 26 29 32 35];
antagonist  = [3 5 7 10 14 17 19 23 27 28 33 36]; % Amisulpride
cond_indx = vertcat(antagonist, placebo, agonist);

% For the difference plot, choose your baseline condition
baseline = 2; % Placebo is baseline
%% Compute ENVELOPE and Resample Source Data
source_data = standardisedata(source_data',cell2mat(T),1);
[source_data, T2] = downsampledata(source_data, cell2mat(T), fs_new, fs);
source_data = source_data';
for i = 1:n_sources % Make sure the filter is passing through each channel and not in other directions
    source_data(i,:) = abs(hilbert(source_data(i,:))).^2;
end
%source_data = abs(hilbert(source_data));


clear data
%% Generate Statemat
% Time and resampled time vector
time = linspace(0,(size(nw_stateseq,2)./fs)-1,size(nw_stateseq,2));
time_new = linspace(0,(size(source_data,2)./fs_new)-1,size(source_data,2));
% Allocating resampled sequence of states
stateseq2 = zeros([1, size(source_data,2)]);
if ~isfile('output/stateseq2_f8-12.mat')
    for ii = 1:size(source_data,2)
        %t = time_new(ii);
        state_indx = find(time_new(ii) >= time,1,'last');
        state = nw_stateseq(state_indx);
        stateseq2(ii) = state;
        disp(['Changing stateseq fs: ' num2str(ii/size(source_data,2) * 100) '%']);
    end
    disp('Changing stateseq fs: Completed...');
else
    load('output/stateseq2_f8-12.mat')
end
% %% Plot states sequences
% plot(time, nw_stateseq); hold on
% plot(time_new, stateseq2);
% xlim([100 120])

%% Create statemat
statemat = zeros([n_states size(stateseq2,2)]);
for ii = 1:n_states
    statemat(ii,:) = (stateseq2 == ii);
end
%% Segment into subjects (sessions) based on T matrix
% Allocate cell arrays for state sequence and source data
s_statemat = cell(1,size(T2,2));
s_source_data = cell(1,size(T,2));

T_sum = cumsum(T2); % cumulative sum

s_source_data{1} = source_data(:,1:T_sum(1));
s_statemat{1} = statemat(:,1:T_sum(1));
for t = 2:size(T2,2)
    s_statemat{t}       = statemat(:,T_sum(t-1)+1:T_sum(t));
    tmp                 = source_data(:,T_sum(t-1)+1:T_sum(t));
    %tmp                 = filterdata(tmp',T2(t),fs,[1,30])';
    %tmp                 = standardisedata(tmp',T2(t),1)';
    s_source_data{t}    = tmp;
end
clear tmp
%% Mean per state per subject
states_s_source_data = {};
mn_states_source_data = {};
mn_states_sources_data = zeros([size(T2,2), n_states, size(source_data,1) ]);
for k = 1:n_states
    for t = 1:size(T2,2)
        indx = find(s_statemat{t}(k,:));
        states_s_source_data{k,t} = s_source_data{t}(:,indx);
        mn_states_sources_data(t,k,:) = mean(s_source_data{t}(:,indx),2);
    end
end

%mn_thrs = mean(reshape(mn_states_sources_data, size(source_data,1), []),1);
%sd_thrs = std(reshape(mn_states_sources_data, size(source_data,1), []),0,1);


mn_thrs = squeeze(mean(mn_states_sources_data,1));
sd_thrs = squeeze(std(mn_states_sources_data,0,1));
%% Calculate threshold
%dist = cell2mat(mn_states_source_data);
%histogram(dist(:),100)
%violin([reshape(dist(:,antagonist),1,[]); reshape(dist(:,placebo),1,[]); reshape(dist(:,agonist),1,[])]')
%confidence = 50;
%thrsh = prctile(dist(:),confidence,2);
% For 60% confidence.
 zthrs = 1.24; % 90% confidence
% confidence = 90;
% mn_state_betas = mean(state_betas,2);
% sd_state_betas = std(state_betas,0,2);
% thrs_betas = prctile(state_betas,confidence,2);
%% Surface plot with percentile threshold from the distribution of all subjects
for i = 1:3 % Antagonist Placebo Agonist
    for k = 1:n_states % States
        
        if i == 1
            condition = 'Antagonist';
        elseif i == 2
            condition = 'Placebo';
        elseif i == 3
            condition = 'Agonist';
        end
        
        %thrs = 1.28;%thrs = prctile(reshape(s_beta(cond_indx(i,:),k,:),1,[]),70);
        tmp = squeeze(mean(mn_states_sources_data(cond_indx(i,:),k,:)));
        %zero_indx = abs(tmp) <= thrs; % thresholding from general distribution
        zero_indx = abs(zscore(tmp)) <= zthrs;
        tmp(zero_indx) = 0;
        betas = tmp(tmp ~= 0);
        areas = dkatlas.tissuelabel(tmp ~= 0)';
        s_table = table(areas, betas);
        writetable(s_table, ['output/areas/' condition '/state_areas_' num2str(k) '_AVG.txt'])
        sourceint.betas = tmp;
        cfg              = [];
        cfg.method       = 'surface';
        cfg.funparameter = 'betas';
        

        %cfg.maskparameter = cfg.funparameter;
        %cfg.funcolorlim   = [-0.01 0.01];
        % cfg.opacitylim    = [3 8];
        %cfg.opacitymap    = 'rampup';

        cfg.colorbar     = 'yes';
        cfg.funcolormap  = '*RdBu';
        figure;ft_sourceplot(cfg,sourceint);
        view([-90 30]);
        %h = light('style','local','position',campos);
        %light('style','infinite','position',[0 -200 200]);
        %lightangle([-90 30]);
        material dull
        set(gcf,'color','white');
        
        savefig(['output/figures/' condition '/meanState_surface_' num2str(k) '_AVG.fig']);
        close all
        
        % Plot from 4 points of view
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([-90 15]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_1_AVG.png'])
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([90 15]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_2_AVG.png'])
        
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([180 0]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_3_AVG.png'])
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([0 0]);
        light('style','infinite','position',[0 -100 200]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_4_AVG.png'])
        
        close all
        
    end
end
clear tmp atmp ptmp percentage betas

%% Condition bar plot
for i = 1:3 % Antagonist Placebo Agonist
    for k = 1:n_states % States
        mn_source = squeeze(mean(mn_states_sources_data(cond_indx(i,:),k,:)));
        sd_source = squeeze(std(mn_states_sources_data(cond_indx(i,:),k,:),0,1));

        zero_indx = abs(zscore(mn_source)) <= 1.28; % 70% thresholding
        mn_source(zero_indx) = 0;
        betas = mn_source(mn_source~=0);
        areas = dkatlas.tissuelabel(mn_source ~= 0)';
        %ylim([-100 100])
        
        bar(categorical(areas),betas);hold on;
        %er = errorbar(1:size(betas,1), betas, errhigh, errlow);
        er = errorbar(betas, sd_source(mn_source~=0)/sqrt(size(cond_indx,2)));
        er.Color = [0 0 0];
        er.LineStyle = 'none';
        title(['State ' num2str(k) ': Mean Activation from lcmv'])
        xlabel('Source Index')
        ylabel('Average Activation')
        
        if i == 1
            condition = 'Antagonist';
        elseif i == 2
            condition = 'Placebo';
        elseif i == 3
            condition = 'Agonist';
        end
        
        saveas(gcf, ['output/figures/' condition '/meanState_bar_' num2str(k) '_AVG.png']);
        close all
    end
end
%% Difference between conditions. Baseline condition

%% Surface plot with percentile threshold from the distribution of all subjects
for i = [1, 3] % Antagonist Placebo Agonist
    for k = 1:n_states % States
        
        if i == 1
            condition = 'Antagonist';
        elseif i == 2
            condition = 'Placebo';
        elseif i == 3
            condition = 'Agonist';
        end
        
        %thrs = 1.28;%thrs = prctile(reshape(s_beta(cond_indx(i,:),k,:),1,[]),70);
        baselineActivity = squeeze(mean(mn_states_sources_data(cond_indx(baseline,:),k,:),1));
        tmp = squeeze(mean(mn_states_sources_data(cond_indx(i,:),k,:)));
        diff = tmp - baselineActivity; % Difference between conditions
        %zero_indx = abs(tmp) <= thrs; % thresholding from general distribution
        zero_indx = abs(zscore(diff)) <= zthrs;
        diff(zero_indx) = 0;
        betas = diff(diff ~= 0);
        areas = dkatlas.tissuelabel(diff ~= 0)';
        s_table = table(areas, betas);
        writetable(s_table, ['output/areas/' condition '/state_areas_' num2str(k) '_AVG_diff.txt'])
        sourceint.betas = diff;
        cfg              = [];
        cfg.method       = 'surface';
        cfg.funparameter = 'betas';
        

        %cfg.maskparameter = cfg.funparameter;
        %cfg.funcolorlim   = [-0.01 0.01];
        % cfg.opacitylim    = [3 8];
        %cfg.opacitymap    = 'rampup';
        cfg.colorbar     = 'yes';
        cfg.funcolormap  = '*RdBu';
        figure;ft_sourceplot(cfg,sourceint);
        view([-90 30]);
        %light('style','infinite','position',[0 -200 200]);
        material dull
        set(gcf,'color','w');
        
        savefig(['output/figures/' condition '/meanState_surface_' num2str(k) '_AVG_diff.fig']);
        close all
        
        % Plot from 4 points of view
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([-90 15]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_1_AVG_diff.png'])
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([90 15]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_2_AVG_diff.png'])
        
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([180 0]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_3_AVG_diff.png'])
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([0 0]);
        light('style','infinite','position',[0 -100 200]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_4_AVG_diff.png'])
        
        close all
        
    end
end
clear tmp atmp ptmp percentage betas


%% AVG Surface plot per state
%histogram(reshape(squeeze(mn_states_sources_data(:,1,:)),1,[]))
% Set threshold
%thrs = prctile(reshape(squeeze(mn_states_sources_data(:,k,:)),1,[]),60); % 60% threshold for maps
for k = 1:n_states % States

    mn_tmp = squeeze(mn_states_sources_data(:,k,:)); % subject x sources
    thrs = prctile(mean(reshape(mn_tmp,[], n_sources),1),90); %only 90 percent activation
    tmp = mean(reshape(mn_tmp,[], n_sources),1);
    %[thrs, indx] = maxk(abs(tmp),5); % Maximum 5 elements
    
    zero_indx = ~(tmp > thrs(1));
    tmp(zero_indx) = 0;
    betas = tmp'; %diff(diff ~= 0);
    areas = dkatlas.tissuelabel(~zero_indx)';
    s_table = table(areas);%, betas(~zero_indx));
    
    % Bar plot
    b = bar(categorical(areas),betas(~zero_indx));hold on
    er = errorbar(betas(~zero_indx),std(mn_tmp(:,~zero_indx),0,1)./sqrt(size(cond_indx,2)));
    er.Color =  'k';
    er.LineStyle = 'none';
    b.FaceColor = [138,43,226]./255;
    %ylim([min(betas(~zero_indx))-0.1, max(betas(~zero_indx))+0.1])
    title('Promedio de activaci�n')
    %xlabel('�reas')
    ylabel('Activaci�n [\muV]','interpreter','Tex');
    set(gcf, 'Color', 'w');
    saveas(gcf, ['output/figures/bar_thresholded/state' num2str(k) '_Activation_AVG.png'])
    close all
    
    % Tabla de texto
    writetable(s_table, ['output/areas/all_subjects/state_areas_' num2str(k) '_AVG.txt'])
    sourceint.betas = tmp';
    cfg              = [];
    cfg.method       = 'surface';
    cfg.funparameter = 'betas';


    cfg.maskparameter = cfg.funparameter;
    %cfg.funcolorlim   = [-0.01 0.01];
    % cfg.opacitylim    = [3 8];
    %cfg.opacitymap    = 'rampup';
    cfg.colorbar     = 'yes';
    cfg.funcolormap  = '*RdBu';
    %cfg.vertexcolor = repmat([1,1,1],[15002,1]);
    %cfg.facecolor = 'brain';
    %cfg.edgecolor = 'brain';
    %cfg.surffile = 'surface_white_left_both.mat';
    %cfg.surfinflated = 'surface_inflated_both.mat';
    figure;ft_sourceplot(cfg,sourceint);
    view([-90 30]);
    %light('style','infinite','position',[0 -200 200]);
    material dull
    set(gcf,'color','w');

    savefig(['output/figures/all_subjects/meanState_surface_' num2str(k) '_AVG.fig']);
    close all

    % Plot from 4 points of view
    cfg.colorbar     = 'no';
    ft_sourceplot(cfg,sourceint);
    material dull
    set(gcf,'color','w');
    view([-90 15]);
    saveas(gcf, ['output/figures/pngs/all_subjects/state' num2str(k) '_surface_view_1_AVG.png'])

    cfg.colorbar     = 'no';
    ft_sourceplot(cfg,sourceint);
    material dull
    set(gcf,'color','w');
    view([90 15]);
    saveas(gcf, ['output/figures/pngs/all_subjects/state' num2str(k) '_surface_view_2_AVG.png'])


    cfg.colorbar     = 'no';
    ft_sourceplot(cfg,sourceint);
    material dull
    set(gcf,'color','w');
    view([180 0]);
    saveas(gcf, ['output/figures/pngs/all_subjects/state' num2str(k) '_surface_view_3_AVG.png'])

    cfg.colorbar     = 'no';
    ft_sourceplot(cfg,sourceint);
    material dull
    set(gcf,'color','w');
    view([0 0]);
    light('style','infinite','position',[0 -100 200]);
    saveas(gcf, ['output/figures/pngs/all_subjects/state' num2str(k) '_surface_view_4_AVG.png'])

    close all

end







