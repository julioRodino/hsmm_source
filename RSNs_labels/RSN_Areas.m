%% atlas 
R_TPN=sum(structural_TPN,3);
[row_TPN,col_TPN] = find(R_TPN);
areas={}
for i = unique(col_TPN)
   areas_TPN= dkatlas.tissuelabel(i);
   disp(i)
end
save('areas_TPN','areas_TPN')

%% Labels
load('labels.mat')
labels_cell=cellstr(labels);
load('structural_VN.mat')
R_VN=sum(structural_VN,3);
[row_VN,col_VN] = find(R_VN);
areas={}
for i = unique(col_VN)
   labels_VN= labels_cell(i);
   disp(i)
end
save('labels_VN','labels_VN')
clear all

%%  Read all lines & collect in cell array
addpath('output/areas/Old')
addpath('Atlas')
%addpath('output/areas/Young')
txtfile=tdfread('state_areas_7_AVG.txt',','); %%%%%%%%%%% cambiar nombre txt
stateareas=cellstr(txtfile.areas);

%% AN
load('labels_AN.mat');
areas_select_AN=ismember(stateareas,labels_AN(:,2));
ind_AN=find(areas_select_AN);
for j=1:length(ind_AN)
  common_areas_AN= (stateareas(ind_AN));
  str_AN=char(common_areas_AN)
end

%% DMN

load('labels_DMN.mat');
areas_select=ismember(stateareas,labels_DMN(:,2));
ind_DMN=find(areas_select);
for j=1:length(ind_DMN)
  common_areas_DMN= (stateareas(ind_DMN));
  str_DMN=char(common_areas_DMN)
end

%% ECN
load('labels_ECN.mat');
areas_select_ECN=ismember(stateareas,labels_ECN(:,2));
ind_ECN=find(areas_select_ECN);
for j=1:length(ind_ECN)
  common_areas_ECN= (stateareas(ind_ECN));
  str_ECN=char(common_areas_ECN)
end

%% FPN
load('labels_FPN.mat');
areas_select_FPN=ismember(stateareas,labels_FPN(:,2));
ind_FPN=find(areas_select_FPN);
for j=1:length(ind_FPN)
  common_areas_FPN= (stateareas(ind_FPN));
  str_FPN=char(common_areas_FPN)
end
%% SMN
load('labels_SMN.mat');
areas_select_SMN=ismember(stateareas,labels_SMN(:,2));
ind_SMN=find(areas_select_SMN);
for j=1:length(ind_SMN)
  common_areas_SMN= (stateareas(ind_SMN));
  str_SMN=char(common_areas_SMN)
end
%% SN
load('labels_SN.mat');
areas_select_SN=ismember(stateareas,labels_SN);
ind_SN=find(areas_select_SN);
for j=1:length(ind_SN)
  common_areas_SN= (stateareas(ind_SN));
  str_SN=char(common_areas_SN)
end
%% TPN
load('labels_TPN.mat');
areas_select_TPN=ismember(stateareas,labels_TPN(:,2));
ind_TPN=find(areas_select_TPN);
for j=1:length(ind_TPN)
  common_areas_TPN= (stateareas(ind_TPN));
  str_TPN=char(common_areas_TPN)
end


%% VN
load('labels_VN.mat');
areas_select_VN=ismember(stateareas,labels_VN(:,2));
ind_VN=find(areas_select_VN);
for j=1:length(ind_VN)
  common_areas_VN= (stateareas(ind_VN));
  str_VN=char(common_areas_VN)
end
%%
%imagesc(sum(structural_FPN,3))