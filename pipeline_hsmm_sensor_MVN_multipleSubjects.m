% pipeline_hsmm_example_MAR2
% Script to perform connectivity estimation using constraints using BSD
% toolbox.

% Steps:
% 1. Add toolbox to path
% 2. Load data
% 3. Define structural constraint
% 4. Initialization
% 5. Define the model
% 6. Train the model
% 7. Verify number of states
% 8. Get results
% 9. Plot state sequences
% 10. Verify error of the state sequence estimation
% 11. Show performance

% HERNAN HERNANDEZ - ALAND ASTUDILLO - JUN 2021

%% 1. Add folders to path
run 'eeglab2019_0/eeglab.m'
addpath(genpath('hsmm31Sep2020'));
addpath(genpath('utils'));
addpath(genpath('HMM-MAR-master 20180107'));
close all
clear all
%% 2. Load data
%load('Data/data_container.mat');
files = dir('data_v2/*.set');
placebo     = [1 4 7 10 14 18 21 22 27 29 33 35 39 41 45 48 50 53 55];
files = files(placebo);
%placebo = [2,6,9,10]; % Index of the placebo files in files variable
%files = dir('C:\Users\J-RC\Documents\Projects\HsMM-D2\D2_modulation\data\preprocessed\epoched2\10*.set');

% Concatenate data and normalize using zscore
data_container.eeg_rest = [];
T= {};
for i = 1:size(files,1)
    EEG                     = pop_loadset(files(i).name,files(i).folder);
    %zdata                   = zscore(double(EEG.data'))';
    zdata                   = zscore(double(EEG.data'))';
    data_container.eeg_rest = horzcat(data_container.eeg_rest,zdata);
    T{i} = [size(zdata,2)];
end

clear zdata % Clear variables
data_container.fs             = 500;
data_container.T              = T;
data_container.subj           = 1;
data_container.block          = 1;
data_container.cond           = 1;
data_container.folder_name    = '10_01_D2_sensorData';
data_container.set_name       = '10_01_D2_sensorData';
data_container.set_mame_block = '10_01_D2_sensorData';
data_container.chanlocs       = EEG.chanlocs;
data_container.chanlocs3      = [];

data_container.data_prepro = data_container.eeg_rest';
data_container = rmfield(data_container,'eeg_rest');
clear EEG
%% 3. Data transformation - transforming the data
% define basic transformation parameters:
tr_options.fs_new = 32; % define subsampling frequency value
tr_options.filter_band = [4 8]; % Define frequency band for filtering
tr_options.do_hilbert = 1; % perform Hilbert transform to obtain envelope signal
tr_options.n_pca_components = 20; % define number of components for PCA
tr_options.do_log = 1;
transform_output = transformData(data_container, tr_options);
T = transform_output.T3ds;
GEEG = transform_output.GEEG;
data = GEEG.data_prepared;

clear data_container tmp transform_output
%% 4. Initialization
d_max = 250; % max duration 
n_states_max = 7; % max number of states 
n_iter = 7; % number of iterations
n_data = size(data, 1); % number of data
%lag = 3;

%[ini2] = util.genera_ini(lag, n_data, d_max, n_iter, n_states_max);

%% 5. Define the model
n_states = 7;
n_channels = size(data,2);
%emisionModel = emis_model.mar3(n_channels, n_states, lag);
emisionModel = emis_model.normal_normal_wishart3(n_channels, n_states);

%durationModel = dur_model.normal_normal_gamma(1, n_states);
durationModel = dur_model.lognormal_normal_gamma(1, n_states);

hsmm3 = hsmm(n_channels, n_states, emisionModel, [], [], durationModel);
hsmm3.priornoinf();

%[mask] = util.convertermar(structural_matrix_prob, n_channels, lag);
%hsmm3.emis_model.prior.coef_mask = mask;
%% Clear unnesesary variables
clear files tr_options 
%% 6. Train the model
tic;
[out_hsmm, sim_array, hsmm_array, model_struct, model_struct_array] = hsmm3.train(data,...
    'maxitersim',8,...
    'tol', 1,...
    'maxcyc', 15,...
    'dmax', 250,...
    'nrep', 6,...
    'parallel', 1,...
    'initoption', 'random2',...
    'ncore', 16);
training_time = toc;
training_output.out_hsmm = out_hsmm;
training_output.sim_array = sim_array;
training_output.hsmm_array = hsmm_array;
training_output.model_struct = model_struct;
training_output.model_struct_array = model_struct_array;
%training_output.model_options = model_options;
training_output.hsmm_instance = hsmm3;
training_output.training_time = training_time;
%% Save training output
save('output/HsMM_output_MVN_sensor_multisubject_fs_64_f4-8_D2.mat','training_output','GEEG','T','-v7.3')
%% 8. Get results
out_hsmm = training_output.out_hsmm;
stateseq = out_hsmm.stateseq;
load('chanlocsD2.mat');
GEEG.chanlocs = chanlocs_D2;
lag = 0;
plotResults;
% %% Save figs
% tempdir = 'figures/';
% FolderName = tempdir;   % Your destination folder
% FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
% for iFig = 1:length(FigList)
%   FigHandle = FigList(iFig);
%   FigName   = get(FigHandle, 'Name');
%   savefig(FigHandle, fullfile(FolderName, [num2str(iFig) '_source_alpha.fig']));
% end
% close all
