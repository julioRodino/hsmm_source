% plot_state_maps.m
% Plot Source Maps with Fieldtrip
%
% Plot Surface State Maps using fieldtrip
% *** This script belongs to the HSMM research. ***
%
% ALAND ASTUDILLO & JULIO RODI�O - APRIL 2022
%% Add and Initialize fieldtrip
addpath('fieldtrip-20210914');
addpath('hsmm31Sep2020');
addpath(genpath('utils'));

ft_defaults % Initialize fieldtrip
%% Conditions matrix
agonist     = [2 6 9 11 13 18 21 22 25 30 31 34]; % Cabergoline
placebo     = [1 4 8 12 15 16 20 24 26 29 32 35];
antagonist  = [3 5 7 10 14 17 19 23 27 28 33 36]; % Amisulpride
cond_indx = vertcat(antagonist, placebo, agonist);

% Version 2 of the model
agonist     = [2 5 9 12 15 17 19 24 25 30 32 34 37 42 43 47 49 52 56]; % Cabergoline
placebo     = [1 4 7 10 14 18 21 22 27 29 33 35 39 41 45 48 50 53 55];
antagonist  = [3 6 8 11 13 16 20 23 26 28 31 36 38 40 44 46 51 54 57]; % Amisulpride
cond_indx = vertcat(antagonist, placebo, agonist);


% Take out outlier subjects for state 4
% agonist     = [2 6 9 11 13 18 22 25 31 34]; % Cabergoline
% placebo     = [1 4 8 12 15 16 24 26 32 35];
% antagonist  = [3 5 7 10 14 17 23 27 33 36]; % Amisulpride
% cond_indx = vertcat(antagonist, placebo, agonist);
%% Load Beta coeficients and create output directories
mkdir('output/figures/agonist');
mkdir('output/figures/placebo');
mkdir('output/figures/antagonist');
mkdir('output/figures/agonist/subjects/');
mkdir('output/figures/placebo/subjects');
mkdir('output/figures/antagonist/subjects');
mkdir('output/figures/pngs/agonist');
mkdir('output/figures/pngs/placebo');
mkdir('output/figures/pngs/antagonist');
mkdir('output/figures/bar');
mkdir('output/figures/bar_thresholded');
mkdir('output/figures/surface');
mkdir('output/areas/agonist');
mkdir('output/areas/antagonist');
mkdir('output/areas/placebo');
mkdir('output/areas/all_subjects');
mkdir('output/figures/pngs/all_subjects');
mkdir('output/figures/HsMM');
%load('output/GLM/GLM_betas_f4-8_D2_V3.mat', 's_beta')
load('output/HsMM_output_MVN_sensor_multisubject_fs_32_f4-8_D2.mat', 'T', 'training_output', 'GEEG')
%% Load Headmodel, Sourcemodel and Atlas. Also Sourceint, ready to plot
files = dir('source_maps_models/*.mat');
for i = 1:size(files,1)
    load([files(i).folder '/' files(i).name])
    disp(['Loaded: ' files(i).name])
end

% %% Plot Surface Brain
% cfg              = [];
% cfg.method       = 'surface';
% cfg.funparameter = 'betas';
% cfg.colorbar     = 'yes';
% cfg.funcolormap  = '*RdBu';
% figure;ft_sourceplot(cfg,sourceint);
% view([-90 30]);
% %light('style','infinite','position',[0 -200 200]);
% material dull
% set(gcf,'color','w');

%% Plot mean State Surface Brain
for i = 1:size(s_beta,2)
    
    tmp = squeeze(mean(s_beta(:,i,:),1));
    atmp =  abs( tmp ./ max(abs(tmp)) ).* 100; % Activation percentage
    zero_indx = abs(zscore(tmp)) <= 1.28; % 90% thresholding
    tmp(zero_indx) = 0; % Set areas lower than 60% to 0
    betas = tmp(tmp ~= 0);
    percentage = atmp(tmp ~= 0);
    areas = dkatlas.tissuelabel(tmp ~= 0)';
    s_table = table(areas, betas, percentage);
    writetable(s_table, ['output/areas/all_subjects/state_areas_' num2str(i) '_GLM.txt'])
    sourceint.betas = tmp;
 

    cfg              = [];
    cfg.method       = 'surface';
    cfg.funparameter = 'betas';

    %cfg.maskparameter = cfg.funparameter;
    cfg.funcolorlim   = [-0.001 0.001];
    % cfg.opacitylim    = [3 8];
    %cfg.opacitymap    = 'rampup';

    cfg.colorbar     = 'yes';
    cfg.funcolormap  = '*RdBu';
    figure(8);ft_sourceplot(cfg,sourceint);
    view([-90 30]);
    %light('style','infinite','position',[0 -200 200]);
    material dull
    set(gcf,'color','w');
    savefig(['output/figures/surface/meanState_surface_' num2str(i) '_GLM.fig']);
    close all
    
    % Plot from 4 different views
    cfg.colorbar     = 'no';
    ft_sourceplot(cfg,sourceint);
    material dull
    set(gcf,'color','w');
    view([-90 15]);
    saveas(gcf, ['output/figures/pngs/all_subjects/state' num2str(i) '_surface_view_1_GLM.png'])

    cfg.colorbar     = 'no';
    ft_sourceplot(cfg,sourceint);
    material dull
    set(gcf,'color','w');
    view([90 15]);
    saveas(gcf, ['output/figures/pngs/all_subjects/state' num2str(i) '_surface_view_2_GLM.png'])


    cfg.colorbar     = 'no';
    ft_sourceplot(cfg,sourceint);
    material dull
    set(gcf,'color','w');
    view([180 0]);
    saveas(gcf, ['output/figures/pngs/all_subjects/state' num2str(i) '_surface_view_3_GLM.png'])

    cfg.colorbar     = 'no';
    ft_sourceplot(cfg,sourceint);
    material dull
    set(gcf,'color','w');
    view([0 0]);
    saveas(gcf, ['output/figures/pngs/all_subjects/state' num2str(i) '_surface_view_4_GLM.png'])

    close all
    
end
clear tmp atmp ptmp percentage betas
%% Plot mean State with error bars THRESHOLDED
% Error bars represent standard error
for i = 1:size(s_beta,2)
    tmp = squeeze(mean(s_beta(:,i,:),1));
    atmp =  abs( tmp ./ max(abs(tmp)) ).* 100; % Activation percentage
    zero_indx = abs(zscore(tmp)) <= 1.28; % 90% thresholding
    tmp(zero_indx) = 0; % Set areas lower than threshold to 0
    betas = tmp(tmp ~= 0);
    areas = dkatlas.tissuelabel(tmp ~= 0)';
    sd_betas = squeeze(std(s_beta(:,i,:),0,1));
    sd_betas = sd_betas(tmp ~= 0);
    stand_error = sd_betas / sqrt(size(s_beta,1));
    %errhigh = betas + stand_error;
    %errlow = betas - stand_error;
    bar(categorical(areas),betas);hold on;
    %er = errorbar(1:size(betas,1), betas, errhigh, errlow);
    er = errorbar(betas, stand_error);
    er.Color = [0 0 0];
    er.LineStyle = 'none';
    title(['State ' num2str(i) ': Betas from GLM'])
    xlabel('Source Index')
    ylabel('Correlation Coef.')
    %ylim([-0.03 0.03])
    saveas(gcf,['output/figures/bar_thresholded/meanState_bar_' num2str(i) '_GLM.png']);
    close all
end

%% Plot mean State with error bars THRESHOLDED per SUBJECT
% Error bars represent standard error
big_areas = zeros([size(s_beta,1) size(s_beta,2)  size(s_beta,3)]);
for s = 1:size(s_beta,1)
    for i = 1:size(s_beta,2)
        [row,col] = find( s == cond_indx);
        
        if row == 1
            condition = 'antagonist';
        elseif row == 2
            condition = 'placebo';
        elseif row == 3
            condition = 'agonist';
        end
        
        tmp = squeeze(s_beta(s,i,:));
        atmp =  abs( tmp ./ max(abs(tmp)) ).* 100; % Activation percentage
        zero_indx = abs(zscore(tmp)) <= 1.28; % 90% thresholding
        tmp(zero_indx) = 0; % Set areas lower than threshold to 0
        betas = tmp(tmp ~= 0);
        areas = dkatlas.tissuelabel(tmp ~= 0)';
        big_areas(s,i,:) = (tmp~=0);
        %sd_betas = squeeze(std(s_beta(:,i,:),0,1));
        %sd_betas = sd_betas(tmp ~= 0);
        %stand_error = sd_betas / sqrt(size(s_beta,1));
        %errhigh = betas + stand_error;
        %errlow = betas - stand_error;
        bar(categorical(areas),betas);
        %er = errorbar(1:size(betas,1), betas, errhigh, errlow);
        %er = errorbar(betas, stand_error);
        %er.Color = [0 0 0];
        %er.LineStyle = 'none';
        title(['State ' num2str(i) ': Betas from GLM'])
        xlabel('Source Index')
        ylabel('Correlation Coef.')
        %ylim([-0.03 0.03])
        saveas(gcf,['output/figures/' condition '/subjects/s_' num2str(col) '_state_' num2str(i) '_areas_GLM.png']);
        close all
    end
end
%% Common areas per state
subplot(1,3,1)
cond_common_areas = squeeze(sum(big_areas(cond_indx(1,:),:,:),1));
bar(cond_common_areas) % Antagonist
title('Antagonist')
subplot(1,3,2)
cond_common_areas = squeeze(sum(big_areas(cond_indx(2,:),:,:),1));
bar(cond_common_areas) % Placebo
title('Placebo')
subplot(1,3,3)
cond_common_areas = squeeze(sum(big_areas(cond_indx(3,:),:,:),1));
bar(cond_common_areas) % Agonist
title('Agonist')
saveas(gcf,'output/figures/common_areas.png')
close all
%% Plot mean State with error bars
for i = 1:size(s_beta,2)
    
    betas = squeeze(mean(s_beta(:,i,:),1));
    sd_betas = squeeze(std(s_beta(:,i,:),0,1));
    stand_error = sd_betas / sqrt(size(s_beta,1));
    bar(betas); hold on;
    er = errorbar(betas, stand_error);
    er.Color = [0 0 0];
    er.LineStyle = 'none';
    title(['State ' num2str(i) ': Betas from GLM'])
    xlabel('Source Index')
    ylabel('Correlation Coef.')
    saveas(gcf,['output/figures/bar/meanState_bar_' num2str(i) '_GLM.png']);
    close all
    
end
%% Plot CONDITION mean State Surface Brain

% 60% threshold from the maximum beta value for that state-condition

for i = 1:3 % Antagonist Placebo Agonist
    for k = 1:size(s_beta,2) % States
        
        if i == 1
            condition = 'Antagonist';
        elseif i == 2
            condition = 'Placebo';
        elseif i == 3
            condition = 'Agonist';
        end
        
        
        tmp = squeeze(mean(s_beta(cond_indx(i,:),k,:),1));
        atmp =  abs( tmp ./ max(abs(tmp)) ).* 100; % Activation percentage
        zero_indx = abs(zscore(tmp)) <= 1.28; % 90% thresholding
        tmp(zero_indx) = 0;
        betas = tmp(tmp ~= 0);
        areas = dkatlas.tissuelabel(tmp ~= 0)';
        percentage = atmp(tmp ~= 0);
        s_table = table(areas, betas, percentage);
        writetable(s_table, ['output/areas/' condition '/state_areas_' num2str(k) '.txt'])
        sourceint.betas = tmp;
        cfg              = [];
        cfg.method       = 'surface';
        cfg.funparameter = 'betas';
        

        cfg.maskparameter = cfg.funparameter;
        cfg.funcolorlim   = [-0.005 0.005];
        % cfg.opacitylim    = [3 8];
        %cfg.opacitymap    = 'rampup';

        cfg.colorbar     = 'yes';
        cfg.funcolormap  = '*RdBu';
        figure;ft_sourceplot(cfg,sourceint);
        view([-90 30]);
        %light('style','infinite','position',[0 -200 200]);
        material dull
        set(gcf,'color','w');
        
        savefig(['output/figures/' condition '/meanState_surface_' num2str(k) '_GLM.fig']);
        close all
        
        % Plot from 4 points of view
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([-90 15]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_1_GLM.png'])
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([90 15]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_2_GLM.png'])
        
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([180 0]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_3_GLM.png'])
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([0 0]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_4_GLM.png'])
        
        close all
        
    end
end
clear tmp atmp ptmp percentage betas
%% Plot CONDITION mean State bars

for i = 1:3 % Antagonist Placebo Agonist
    for k = 1:size(s_beta,2) % States

        sd_betas = squeeze(std(s_beta(cond_indx(i,:),k,:),0,1));
        tmp = squeeze(mean(s_beta(cond_indx(i,:),k,:),1));
        ptmp = (tmp ./ max(abs(tmp)) ).* 100;
        atmp =  abs( tmp ./ max(abs(tmp)) ).* 100; % Activation percentage
        zero_indx = abs(zscore(tmp)) <= 1.28; % 90% thresholding
        tmp(zero_indx) = 0;
        stand_error = sd_betas(tmp~=0) / sqrt(size(s_beta,1));
        betas = tmp(tmp~=0);
        areas = dkatlas.tissuelabel(tmp ~= 0)';
        %ylim([-100 100])
        
        bar(categorical(areas),betas);hold on;
        %er = errorbar(1:size(betas,1), betas, errhigh, errlow);
        er = errorbar(betas, stand_error);
        er.Color = [0 0 0];
        er.LineStyle = 'none';
        title(['State ' num2str(k) ': Betas from GLM'])
        xlabel('Source Index')
        ylabel('Correlation Coef.')
        
        if i == 1
            condition = 'Antagonist';
        elseif i == 2
            condition = 'Placebo';
        elseif i == 3
            condition = 'Agonist';
        end
        
        saveas(gcf, ['output/figures/' condition '/meanState_bar_' num2str(k) '_GLM.png']);
        close all
    end
end

%% Fractional Occupancy Total

fs_new = GEEG.fs_new;
tateseq = training_output.out_hsmm.stateseq;
training_output.out_hsmm.nstates;
[FO, DurationCat] = seqMeasures2(stateseq, nstates);
%bar(FO);
clear GEEG
%% Fractional Occupancy Per subject
stateseq = training_output.out_hsmm.stateseq;
nstates = training_output.out_hsmm.nstates;
big_fo = zeros([size(T,1) nstates]);
big_durCat = {};
T_sum = cumsum(T);
s_stateseq = stateseq(1:T(1));
[FO, DurationCat] = seqMeasures2(s_stateseq, nstates);
big_fo(1,:) = FO;
big_durCat{1} = DurationCat;
bar(FO);
title(['FO% for subject ' '1'])
ylabel('FO%')
xlabel('Network')
ylim([0 35])
saveas(gcf, ['output/figures/HsMM2/FO_subject' '1' '.png']);
close all

 
for s = 2:size(T,1)
    s_stateseq = stateseq(T_sum(s-1):T_sum(s));
    [FO, DurationCat] = seqMeasures2(s_stateseq, nstates);
    big_fo(s,:) = FO;
    big_durCat{s} = DurationCat;
    bar(FO);
    title(['FO% for subject ' num2str(s)])
    ylabel('FO%')
    xlabel('Network')
    ylim([0 35])
    saveas(gcf, ['output/figures/HsMM2/FO_subject' num2str(s) '.png']);
    close all
end

%% FO per condition
FO_antagonist = big_fo(antagonist,:);
FO_placebo    = big_fo(placebo,:);
FO_agonist    = big_fo(agonist,:);

for k = 1:nstates
    total_fo = [FO_antagonist(:,k), FO_placebo(:,k), FO_agonist(:,k)];
    csvwrite(['output/fo_state_' num2str(k) '_ver2.csv'],total_fo);
end
%% ANOVA FO
[p,tbl,stats] = anova1(log(total_fo));
multcompare(stats)

[p,tbl,stats] = kruskalwallis(total_fo)
multcompare(stats)
%close all
%% T-Test FO
[h,p] = ttest(FO_agonist(:,6), FO_placebo(:,6));disp(p)
[h,p] = ttest(FO_antagonist(:,6), FO_placebo(:,6));disp(p)

%% Difference in variability test
k = 6;disp(['k =' num2str(k)])
[h,p] = vartest2(FO_placebo(:,k),FO_antagonist(:,k));
disp(['Placebo V Antagonist: p = ' num2str(p)])

[h,p] = vartest2(FO_placebo(:,k),FO_agonist(:,k));
disp(['Placebo V Agonist: p = ' num2str(p)])
%% FO distributions per state
for k = 1: nstates
    c = [0.15 0.1 0.3; 0.2 0.5 0.7; 0.6 0.5 0.7];
    violin([FO_antagonist(:,k),FO_placebo(:,k), FO_agonist(:,k)],...
        'facecolor',c,...
        'xlabel', {'Antagonista', 'Placebo', 'Agonista'})
    title(['Ocupancia Fraccional de Estado: ' num2str(k)])
    ylabel('FO%')
    saveas(gcf,['output/figures/HsMM2/FO_violin_s' num2str(k) '.png'])
    close all
end
%% FO Plots violin 2
for k = 1: nstates
    violinplot([FO_antagonist(:,k),FO_placebo(:,k), FO_agonist(:,k)],...
        {'Antagonista', 'Placebo', 'Agonista'},...
        'ViolinColor', c,...
        'EdgeColor', [0,0,0],...
        'BoxColor', [0,0,0],...
        'MedianColor', [0.8,0,0.2],...
        'ShowMean', true,...
        'Width', 0.3,...
        'QuartileStyle','boxplot',...
        'DataStyle', 'scatter')
    title(['Ocupancia Fraccional de Estado: ' num2str(k)])
    ylabel('FO%')
    ylim([0 35])
    saveas(gcf,['output/figures/HsMM2/FO_violin2_s' num2str(k) '.png'])
    close all
end
%% Normality test

kstest(zscore(FO_antagonist(:,4)))
lillietest(FO_antagonist(:,7))
%% Kuskalwallis test
state = 4;
[p,tbl,stats] = kruskalwallis([FO_antagonist(:,state), FO_placebo(:,state), FO_agonist(:,state)]);
multcompare(stats)



%% Test for difference between FO% in ag and ant compared to placebo
ant_diff = FO_antagonist - FO_placebo;
an_diff = FO_agonist - FO_placebo;
bar([mean(ant_diff,1); mean(an_diff,1)]');

%% FO Variability per condition
boxplot(FO_antagonist)
xlabel('States')
ylabel('FO%')
ylim([0 36])
title('Fractional Occupancy Variability')
saveas(gcf, 'output/figures/HsMM2/FO_variability_antagonist.png');
close all
boxplot(FO_placebo)
xlabel('States')
ylabel('FO%')
ylim([0 36])
title('Fractional Occupancy Variability')
saveas(gcf,'output/figures/HsMM2/FO_variability_placebo.png');
close all
boxplot(FO_agonist)
title('Fractional Occupancy Variability')
xlabel('States')
ylabel('FO%')
ylim([0 36])
saveas(gcf, 'output/figures/HsMM2/FO_variability_agonist.png');
close all

%% FO per condition
mn_FO = [mean(FO_antagonist,1); mean(FO_placebo,1); mean(FO_agonist,1)]';
b = bar(mn_FO, 'FaceColor', 'Flat'); hold on
legend('Antagonist', 'Placebo', 'Agonist')
sd_FO = [std(FO_antagonist,0,1); std(FO_placebo,0,1); std(FO_agonist,0,1)]';
legend('Antagonist', 'Placebo', 'Agonist')

% Calculate the number of groups and number of bars in each group
[ngroups,nbars] = size(mn_FO);
% Get the x coordinate of the bars
x = nan(nbars, ngroups);
for i = 1:nbars
    x(i,:) = b(i).XEndPoints;
end
% Plot the errorbars
errorbar(x',mn_FO,sd_FO,'k','linestyle','none');
legend('Antagonist', 'Placebo', 'Agonist')

c = [0.15 0.1 0.3; 0.2 0.5 0.7; 0.6 0.5 0.7]; % Colors for each condition
for k = 1:size(mn_FO,2)
    b(k).CData = c(k,:);
end
hold off
xlabel('States')
ylabel('FO%')
%ylim([10 20]);
title('Fractional Occupancy')
saveas(gcf, 'output/figures/HsMM2/FO_condition.png')
close all
%% FO per condition errorbars as standard error
f = figure('Position', [0 0 700 300]);
mn_FO = [mean(FO_antagonist,1); mean(FO_placebo,1); mean(FO_agonist,1)]';
b = bar(mn_FO, 'FaceColor', 'Flat'); hold on
legend('Antagonist', 'Placebo', 'Agonist')
sd_FO = [std(FO_antagonist,0,1); std(FO_placebo,0,1); std(FO_agonist,0,1)]';
legend('Antagonist', 'Placebo', 'Agonist')

% Calculate the number of groups and number of bars in each group
[ngroups,nbars] = size(mn_FO);
% Get the x coordinate of the bars
x = nan(nbars, ngroups);
for i = 1:nbars
    x(i,:) = b(i).XEndPoints;
end
% Plot the errorbars
errorbar(x',mn_FO,sd_FO/sqrt(size(cond_indx,2)),'k','linestyle','none');
legend('Antagonista D2', 'Placebo', 'Agonista D2')

c = [0.15 0.1 0.3; 0.2 0.5 0.7; 0.6 0.5 0.7]; % Colors for each condition
for k = 1:size(mn_FO,2)
    b(k).CData = c(k,:);
end
hold off
xlabel('Estado')
ylabel('FO%')
ylim([10 22])
title('Ocupaci�n Fraccional')
set(gcf,'color','w');
saveas(f, 'output/figures/HsMM2/FO_condition_standError.png')
close all

%% State durations per condition
durCat_antagonist = big_durCat{antagonist};
durCat_placebo = big_durCat{placebo};
durCat_agonist = big_durCat{agonist};

boxplot(durCat_antagonist(:, 1)/fs_new, durCat_antagonist(:, 2), 'notch', 'on'); 
ylabel('Duration s', 'FontSize', 12); xlabel('State', 'FontSize', 12);
ylim([0 1.2])
title('Antagonist Condition Durations')
saveas(gcf,'output/figures/HsMM2/durations_boxplot_antagonist.png')
close all

boxplot(durCat_placebo(:, 1)/fs_new, durCat_placebo(:, 2), 'notch', 'on'); 
ylabel('Duration s', 'FontSize', 12); xlabel('State', 'FontSize', 12);
title('Placebo Condition Empirical Durations')
ylim([0 1.2])
saveas(gcf,'output/figures/HsMM2/durations_boxplot_placebo.png')
close all

boxplot(durCat_agonist(:, 1)/fs_new, durCat_agonist(:, 2), 'notch', 'on'); 
ylabel('Duration s', 'FontSize', 12); xlabel('State', 'FontSize', 12);
title('Agonist Condition Empirical Durations')
ylim([0 1.2])
saveas(gcf,'output/figures/HsMM2/durations_boxplot_agonist.png')
close all

%% Variability and distribution of state durations per condition
c_durCat_antagonist = {};
c_durCat_placebo = {};
c_durCat_agonist = {};

c = [0.15 0.1 0.3; 0.2 0.5 0.7; 0.6 0.5 0.7];
for k = 1:nstates
    c_durCat_antagonist{k} = durCat_antagonist(find(durCat_antagonist(:,2) == k),1)./fs_new;
    c_durCat_placebo{k} = durCat_placebo(find(durCat_placebo(:,2) == k),1)./fs_new;
    c_durCat_agonist{k} = durCat_agonist(find(durCat_agonist(:,2) == k),1)./fs_new;
end
violin(c_durCat_antagonist, 'facecolor', c(1,:));
ylabel('Duraci�n (s)', 'FontSize', 12); xlabel('Estado', 'FontSize', 12);
title('Condici�n Antagonista D2: Distribuci�n de Duraciones emp�ricas')
ylim([-0.1 1.2]);
saveas(gcf,'output/figures/HsMM2/durations_violin_antagonist.png')
close all
violin(c_durCat_placebo, 'facecolor', c(2,:))
ylabel('Duraci�n (s)', 'FontSize', 12); xlabel('Estado', 'FontSize', 12);
title('Condici�n Placebo: Distribuci�n de Duraciones emp�ricas')
ylim([-0.1 1.2]);
saveas(gcf,'output/figures/HsMM2/durations_violin_placebo.png')
close all
violin(c_durCat_agonist, 'facecolor', c(3,:))
ylabel('Duraci�n (s)', 'FontSize', 12); xlabel('Estado', 'FontSize', 12);
title('Condici�n Agonista D2: Distribuci�n de Duraciones emp�ricas')
ylim([-0.1 1.2]);
saveas(gcf,'output/figures/HsMM2/durations_violin_agonist.png')
close all

%% Plot the tail (standard deviation) of Duration's empirical log-normal distribution
tail_agonist = zeros([2,nstates]);
tail_placebo = zeros([2,nstates]);
tail_antagonist = zeros([2,nstates]);

s_tail = zeros([size(big_durCat,2), nstates, 2]); % Last dimension is 1 for mean and 2 for variance

for s = 1:size(big_durCat,2)
    for k = 1:nstates
        s_tail(s,k,:) = lognfit(big_durCat{s}(big_durCat{s}(:,2)==k,1));
    end
end

mn_tail_antagonist = zeros([1,nstates]);
mn_tail_placebo = zeros([1,nstates]);
mn_tail_agonist = zeros([1,nstates]);

sd_tail_antagonist = zeros([1,nstates]);
sd_tail_placebo = zeros([1,nstates]);
sd_tail_agonist = zeros([1,nstates]);

for k = 1:nstates
    mn_tail_antagonist(1,k) = mean(s_tail(antagonist,k,2));
    sd_tail_antagonist(1,k) = std(s_tail(antagonist,k,2),0,1);
    
    mn_tail_placebo(1,k) = mean(s_tail(placebo,k,2));
    sd_tail_placebo(1,k) = std(s_tail(placebo,k,2),0,1);
    
    mn_tail_agonist(1,k) = mean(s_tail(agonist,k,2));
    sd_tail_agonist(1,k) = std(s_tail(agonist,k,2),0,1);
    
end

mn_tail = [mn_tail_antagonist; mn_tail_placebo; mn_tail_agonist]';
mn_tail = (mn_tail ./ fs_new)*1000;
sd_tail = [sd_tail_antagonist; sd_tail_placebo; sd_tail_agonist]';
sd_tail = (sd_tail ./ fs_new)*1000;
b = bar(mn_tail, 'FaceColor', 'Flat'); hold on
legend('Antagonist', 'Placebo', 'Agonist')

legend('Antagonist', 'Placebo', 'Agonist')

% Calculate the number of groups and number of bars in each group
[ngroups,nbars] = size(mn_tail);
% Get the x coordinate of the bars
x = nan(nbars, ngroups);
for i = 1:nbars
    x(i,:) = b(i).XEndPoints;
end
% Plot the errorbars
errorbar(x',mn_tail,sd_tail./sqrt(s/3),'k','linestyle','none');
% Plot std
%errorbar(x',mn_tail,sd_tail,'k','linestyle','none');
legend('Antagonista', 'Placebo', 'Agonista')

c = [0.15 0.1 0.3; 0.2 0.5 0.7; 0.6 0.5 0.7]; % Colors for each condition
for k = 1:size(mn_tail,2)
    b(k).CData = c(k,:);
end
hold off
xlabel('Estado')
ylabel('Standard Deviation log-normal empirical distribution (ms)')
title('Standard Deviation from log-normal empirical duration distribution')
ylim([15 20])
%saveas(gcf, 'output/figures/HsMM2/durationsTail_Bar_errstd.png')
%close all


%% Comparing distributions of BS between conditions
violinplot({c_durCat_antagonist{k} c_durCat_placebo{k}, c_durCat_agonist{k}})
%%
boxplot(c_durcat_agonist)
%% Duration Dist Subplot of 3 conditions
subplot(3,1,1)
violin(c_durCat_antagonist, 'facecolor', c(1,:));
%ylabel('Duraci�n (s)', 'FontSize', 12); xlabel('Estado', 'FontSize', 12);
title('Antagonist Condition Empirical Durations')
ylim([-0.1 1.2]);
set(gca,'xtick',[])
subplot(3,1,2)
violin(c_durCat_placebo, 'facecolor', c(2,:))
ylabel('Duraci�n (s)', 'FontSize', 12); %xlabel('Estado', 'FontSize', 12);
title('Placebo Condition Empirical Durations')
ylim([-0.1 1.2]);
subplot(3,1,3)
violin(c_durCat_agonist, 'facecolor', c(3,:))
%ylabel('Duraci�n (s)', 'FontSize', 12); 
xlabel('Estado', 'FontSize', 12);
title('Agonist Condition Empirical Durations')
ylim([-0.1 1.2]);
close all

%% Durations per condition with errorbars as standard error
f = figure('Position', [0 0 700 300]);
mn_durCat_antagonist = zeros([1 nstates]);
mn_durCat_placebo = zeros([1 nstates]);
mn_durCat_agonist = zeros([1 nstates]);
sd_durCat_antagonist = zeros([1 nstates]);
sd_durCat_placebo = zeros([1 nstates]);
sd_durCat_agonist = zeros([1 nstates]);
for k = 1:nstates
    mn_durCat_antagonist(k) = mean(durCat_antagonist(find(durCat_antagonist(:,2) == k),1))./fs_new;
    sd_durCat_antagonist(k) = std(durCat_antagonist(find(durCat_antagonist(:,2) == k),1))./fs_new;
    
    mn_durCat_placebo(k) = mean(durCat_placebo(find(durCat_placebo(:,2) == k),1))./fs_new;
    sd_durCat_placebo(k) = std(durCat_placebo(find(durCat_placebo(:,2) == k),1))./fs_new;
    
    mn_durCat_agonist(k) = mean(durCat_agonist(find(durCat_agonist(:,2) == k),1))./fs_new;
    sd_durCat_agonist(k) = std(durCat_agonist(find(durCat_agonist(:,2) == k),1))./fs_new;
end

mn_dur = [mn_durCat_antagonist; mn_durCat_placebo; mn_durCat_agonist]';
b = bar(mn_dur, 'FaceColor', 'Flat'); hold on
legend('Antagonist', 'Placebo', 'Agonist')
sd_dur = [sd_durCat_antagonist; sd_durCat_placebo; sd_durCat_agonist]';
legend('Antagonist', 'Placebo', 'Agonist')

% Calculate the number of groups and number of bars in each group
[ngroups,nbars] = size(mn_dur);
% Get the x coordinate of the bars
x = nan(nbars, ngroups);
for i = 1:nbars
    x(i,:) = b(i).XEndPoints;
end
% Plot the errorbars
errorbar(x',mn_dur,sd_dur/sqrt(12),'k','linestyle','none');
legend('Antagonista', 'Placebo', 'Agonista')

c = [0.15 0.1 0.3; 0.2 0.5 0.7; 0.6 0.5 0.7]; % Colors for each condition
for k = 1:size(mn_dur,2)
    b(k).CData = c(k,:);
end
hold off
xlabel('Estado')
ylabel('Duraci�n (s)')
title('Duraci�n Promedio')
ylim([0.1 0.23])
saveas(gcf, 'output/figures/HsMM2/durations_Bar_errstd.png')
close all


%% State Durations inter-subject variability
for k = 1:nstates
    for s = 1:size(big_durCat,2)
        k_indx = (big_durCat{s}(:,2) == k);
        durations{k,s} = mean(big_durCat{s}(k_indx,1))./fs_new;
    end
    org_durations = [durations{k,antagonist}; durations{k,placebo}; durations{k,agonist}];
    violinplot(org_durations',...
    {'Antagonista', 'Placebo', 'Agonista'},...
    'ViolinColor', c,...
    'EdgeColor', [0,0,0],...
    'BoxColor', [0,0,0],...
    'MedianColor', [0.8,0,0.2],...
    'ShowMean', true,...
    'Width', 0.3,...
    'QuartileStyle','boxplot',...
    'DataStyle', 'scatter')
    title(['Duraci�n promedio de Estado: ' num2str(k)])
    ylabel('Duraci�n [s]')
    ylim([0 0.4])
    saveas(gcf,['output/figures/HsMM2/dur_violin_s' num2str(k) '.png'])
    close all
    csvwrite(['output/state_' num2str(k) '_dur_ver2.csv'],org_durations');
end

%% Durations variability test
for k = 1:nstates
    disp(['F-test para estado ' num2str(k)])
    [h,p] = vartest2([durations{k,antagonist}],[durations{k,placebo}]);
    disp(['Antagonista - Placebo: ' num2str(p)])
    [h,p] = vartest2([durations{k,agonist}],[durations{k,placebo}]);
    disp(['Agonista - Placebo: ' num2str(p)])
end

%% Mean Duration Plot
states_mn_avgDur = zeros([nstates,size(cond_indx,1)]);
states_sd_avgDur = zeros([nstates,size(cond_indx,1)]);
for k = 1:nstates
    
    mn_avgDur = mean([durations{k,antagonist};durations{k,placebo}; durations{k,agonist}],2);
    sd_avgDur = std([durations{k,antagonist};durations{k,placebo}; durations{k,agonist}],0,2);
    states_mn_avgDur(k,:) = mn_avgDur;
    states_sd_avgDur(k,:) = sd_avgDur;
end

f = figure('Position', [0 0 700 300]);
b = bar(states_mn_avgDur, 'FaceColor', 'Flat'); hold on
legend('Antagonist', 'Placebo', 'Agonist')

% Calculate the number of groups and number of bars in each group
[ngroups,nbars] = size(states_mn_avgDur);
% Get the x coordinate of the bars
x = nan(nbars, ngroups);
for i = 1:nbars
    x(i,:) = b(i).XEndPoints;
end
% Plot the errorbars
errorbar(x',states_mn_avgDur,states_sd_avgDur/sqrt(size(cond_indx,2)),'k','linestyle','none');
legend('Antagonista D2', 'Placebo', 'Agonista D2')

c = [0.15 0.1 0.3; 0.2 0.5 0.7; 0.6 0.5 0.7]; % Colors for each condition
for k = 1:size(states_mn_avgDur,2)
    b(k).CData = c(k,:);
end
hold off
xlabel('Estado')
ylabel('FO%')
ylim([0.1 0.25])
title('Duraci�n Promedio')
set(gcf,'color','w');
saveas(f, 'output/figures/HsMM/avgDuraci�n_stdErr.png')
close all


%% Surface plot per condition based on 5% of all areas in all subjects per state
state_betas = [];
c2 = [0.2 0.5 0.5; 0.6 0.3 0.5; 0.2 0.7 0.1; 0.1 0.1 0.7; 0.7 0.1 0.3;...
    0.5 0.7 0.4; 0.1 0.9 0.7]; % Color for histogram
figure('Position', [0 0 1920/2 780/2])
%% Histogram
for k = 1:nstates
    state_betas(k,:) = reshape(squeeze(s_beta(:,k,:)),1,[]);
    subplot(1,nstates,k)
    
    h = histfit(zscore(state_betas(k,:)),200);
    h(1).FaceColor =  c2(k,:); % Color for bar fit
    h(2).Color =  [0.4 0.2 0.6]; % Color for line fit
    h(1).EdgeAlpha = 0; % Transparency of bin outline
    title(['State: ' num2str(k)]) 
    xlabel('Correlation Coef.');
    if k == 1
        ylabel('Frequency');
    end
end
saveas(gcf,'output/figures/states_Beta_histogram_GLM.png')
close all
%% Calculate threshold
z_thrsh = 1.24; % 90% confidence
confidence = 70;
mn_state_betas = mean(state_betas,2);
sd_state_betas = std(state_betas,0,2);
thrs_betas = prctile(state_betas,confidence,2);
% %% Surface plot with percentile threshold from the distribution of all subjects
% for i = 1:3 % Antagonist Placebo Agonist
%     for k = 1:size(s_beta,2) % States
%         
%         if i == 1
%             condition = 'Antagonist';
%         elseif i == 2
%             condition = 'Placebo';
%         elseif i == 3
%             condition = 'Agonist';
%         end
%         
%         thrs = z_thrsh;%thrs = prctile(reshape(s_beta(cond_indx(i,:),k,:),1,[]),70);
%         tmp = squeeze(mean(s_beta(cond_indx(i,:),k,:),1));
%         zero_indx = abs(zscore(tmp)) <= thrs; % thresholding from general distribution
%         tmp(zero_indx) = 0;
%         betas = tmp(tmp ~= 0);
%         areas = dkatlas.tissuelabel(tmp ~= 0)';
%         s_table = table(areas, betas);
%         writetable(s_table, ['output/areas/' condition '/state_areas_' num2str(k) '.txt'])
%         sourceint.betas = tmp;
%         cfg              = [];
%         cfg.method       = 'surface';
%         cfg.funparameter = 'betas';
%         
% 
%         cfg.maskparameter = cfg.funparameter;
%         cfg.funcolorlim   = [-0.01 0.01];
%         % cfg.opacitylim    = [3 8];
%         %cfg.opacitymap    = 'rampup';
% 
%         cfg.colorbar     = 'yes';
%         cfg.funcolormap  = '*RdBu';
%         figure;ft_sourceplot(cfg,sourceint);
%         view([-90 30]);
%         %light('style','infinite','position',[0 -200 200]);
%         material dull
%         set(gcf,'color','w');
%         
%         savefig(['output/figures/' condition '/meanState_surface_' num2str(k) '.fig']);
%         close all
%         
%         % Plot from 4 points of view
%         cfg.colorbar     = 'no';
%         ft_sourceplot(cfg,sourceint);
%         material dull
%         set(gcf,'color','w');
%         view([-90 15]);
%         saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_1.png'])
%         
%         cfg.colorbar     = 'no';
%         ft_sourceplot(cfg,sourceint);
%         material dull
%         set(gcf,'color','w');
%         view([90 15]);
%         saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_2.png'])
%         
%         
%         cfg.colorbar     = 'no';
%         ft_sourceplot(cfg,sourceint);
%         material dull
%         set(gcf,'color','w');
%         view([180 0]);
%         saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_3.png'])
%         
%         cfg.colorbar     = 'no';
%         ft_sourceplot(cfg,sourceint);
%         material dull
%         set(gcf,'color','w');
%         view([0 0]);
%         saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_4.png'])
%         
%         close all
%         
%     end
% end
% clear tmp atmp ptmp percentage betas
% 
