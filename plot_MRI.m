ft_defaults
%%
% Load template MRI
MRI = ft_read_mri('standard_mri.mat');
dkatlas = ft_read_atlas('dkatlas.mat');
dkatlas.coordsys = 'acpc';
labels = dkatlas.tissuelabel;
ft_convert_coordsys(dkatlas,'spm');
nw_MRI = ft_convert_coordsys(MRI,'acpc');

cfg = [];
cfg.nonlinear = 'yes';
cfg.parameter = 'tissue';
dkatlas = ft_sourceinterpolate(cfg,dkatlas,nw_MRI);
dkatlas.tissuelabel = labels;
cfg = [];
cfg.atlas = dkatlas;
ft_sourceplot(cfg,nw_MRI)

