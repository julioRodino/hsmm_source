function [mapMeans, mapDists] = getMeanMap(Bmaps)
nsubjects = size(Bmaps,2);
nStates = size(Bmaps{1},1);
ndim = size(Bmaps{1},2);

%% GET THE MEAN MAPS FOR EACH STATE
BMmeanC = {};
for kstate = 1:nStates  %options.K
    BMmean = zeros(1,60);
    for ksubject = 1:nsubjects
        BM = Bmaps{ksubject};
        stmap = BM(kstate,:) ;
        BMmean = BMmean + stmap;
    end
    BMmeanC{kstate} = BMmean/nsubjects;
end


%% GET EUCLIDIAN DISTANCE OF EACH MAP
dMap = [];
for ksubject = 1:nsubjects
    BM = Bmaps{ksubject};
    for kstate = 1:nStates  %options.K
        stmap = BM(kstate,:) ;
        dMap(ksubject,kstate) = sqrt(sum(stmap.^2));
    end
end
mapMeans = BMmeanC;
mapDists = dMap;
