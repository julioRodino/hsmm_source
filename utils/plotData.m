% plotData
% Script to plot data and transformation steps results.
%
% ALAND ASTUDILLO JUN 2021
data_cell{1} = data_container.data_prepro;

transform_options = transform_output.transform_options;
transform_options2 = transform_output.transform_options2;
data_transformed = transform_output.data_transformed; 
data_steps = transform_output.data_steps;
do_log = transform_output.do_log; % rescaling using simple Log transformation
do_post_ref = transform_output.do_post_ref; % average re-reference after log, uses the reref command from Eeglab!
mantain_steps = transform_output.mantain_steps;

doplot1 = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

%% FIGURE STEPS
% data preprocessed
if doplot1(1)
kset = 1;
X = data_cell{1, kset};
durs = data_steps{1, 1}.T;
dursSum = cumsum(durs);
tv = (0:size(X, 1) - 1)/data_container.fs;
x_prepro = data_cell{1, 1}; 
% filtered
X = data_steps{1, 1}.D.X; 
x_filtered = X(1:dursSum(1), :);
% standardized
X = data_steps{1, 3}.D.X; % Standardized data
x_standard = X(1:dursSum(1), :);
% envelope
X = data_steps{1, 4}.D.X; % Envelope data
x_envelope = X(1:dursSum(1), :);
% log rescale
X = data_steps{1, 5}.D.X; % Log data
x_log = X(1:dursSum(1), :);
% PCA data
X = data_steps{1, 6}.D.X; % PCA data
x_pca = X(1:dursSum(1), :);

xlimite = [0, 20];
ylimite = [0, 5];
kchannel = 1;
nchannels = 3;
amp1 = 0.025;
amp2 = 0.25;
amp3 = 0.1;
amp4 = 0.5;

figure;
subplot(3,1,1); hold on;
for kchannel = 1:nchannels
%     plot(tv, amp1 * x_prepro(:, kchannel) + kchannel - 1);
    plot(tv, amp1 * x_filtered(:, kchannel) + kchannel - 1);
end
xlim(xlimite); set(gca, 'FontSize', 12);
title('Signal and transformation steps', 'FontSize', 14);
xlabel('Time s', 'FontSize', 12); ylabel('Filtered', 'FontSize', 12); 

subplot(3,1,2); hold on;
for kchannel = 1:nchannels
    plot(tv, amp2 * x_standard(:, kchannel) + kchannel - 1);
    plot(tv, amp2 * x_envelope(:, kchannel) + kchannel - 1);
end
xlabel('Time s', 'FontSize', 12); ylabel('Envelope', 'FontSize', 12); 
xlim(xlimite); set(gca, 'FontSize', 12);

subplot(3,1,3); hold on;
for kchannel = 1:nchannels
%     plot(tv, amp3 * x_log(:, kchannel) + kchannel - 1);
    plot(tv, amp3 * x_pca(:, kchannel) + kchannel - 1);
end
xlabel('Time s', 'FontSize', 12); ylabel('PCA', 'FontSize', 12); 
xlim(xlimite); set(gca, 'FontSize', 12);
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: PLOT DATA BEFORE PREPARATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(2)
X = [];
nSets = 1; % though for more than 1 block
xlimite_dist = [-25, 25];
ylimite_signal = [-100, 100];
for kset = 1:nSets
    X = [X; data_cell{1, kset}];
end
tv = (0:size(X, 1) - 1)/data_container.fs;
durs = data_steps{1, 1}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = data_cell{1, 1}; 
% x2 = dataR{1, 2}; %X(1 + dursSum(1):dursSum(2), :);
% x3 = dataR{1, 3}; %X(1 + dursSum(2):dursSum(3), :);

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
title('Input Signals'); xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg) 
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2); 
end
xlim([0, tv(end)]); ylim(ylimite_signal);
% text(40, 70, 'S1'); text(40*3, 70, 'S2'); text(40*6, 70, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist);
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE : CHECK DATA RANGE IN EACH PRE PROCESING PIPE STEP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(3)
% FILTERED DATA
ylimite_signal = [-100, 100];
xlimite_dist = [-25, 25];
X = data_steps{1, 1}.D.X; 
tv = (0:size(X, 1) - 1)/data_container.fs;
durs = data_steps{1, 1}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :);
% x2 = X(1 + dursSum(1):dursSum(2), :);
% x3 = X(1 + dursSum(2):dursSum(3), :);

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
title('Filtered Signals'); xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg)
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2); 
end
xlim([0, tv(end)]); ylim(ylimite_signal);
% text(40, 70, 'S1'); text(40*3, 70, 'S2'); text(40*6, 70, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist);
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist);
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist);
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: Detrended Stage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(4)
ylimite_signal = [-100, 100];
xlimite_dist = [-25, 25];
X = data_steps{1, 2}.D.X; % Detrended data
tv = (0:size(X, 1) - 1)/data_container.fs;
durs = data_steps{1, 2}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :);
% x2 = X(1 + dursSum(1):dursSum(2), :);
% x3 = X(1 + dursSum(2):dursSum(3), :);

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
title('Detrended Signals'); xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg) 
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2); 
end
xlim([0, tv(end)]); ylim(ylimite_signal);
% text(40, 70, 'S1'); text(40*3, 70, 'S2'); text(40*6, 70, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist);
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist);
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist);
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: Standardized Stage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(5)
ylimite_signal = [-8, 8];
X = data_steps{1, 3}.D.X; % Standardized data
tv = (0:size(X, 1) - 1)/data_container.fs;
durs = data_steps{1, 3}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :);
% x2 = X(1 + dursSum(1):dursSum(2), :);
% x3 = X(1 + dursSum(2):dursSum(3), :);
xlimite_dist = [min(X(:)), max(X(:))];

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
title('Standardized Signals'); xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg)
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2); 
end
xlim([0, tv(end)]); ylim(ylimite_signal); %[-8, 8]);
% text(40, 7, 'S1'); text(170, 7, 'S2'); text(300, 7, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist); 
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist); 
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist); 
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: Envelope Stage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(6)
ylimite_signal = [-8, 8];
X = data_steps{1, 4}.D.X; % Envelope data
tv = (0:size(X, 1) - 1)/data_container.fs;
durs = data_steps{1, 4}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :);
% x2 = X(1 + dursSum(1):dursSum(2), :);
% x3 = X(1 + dursSum(2):dursSum(3), :);
xlimite_dist = [min(X(:)), max(X(:))];

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
title('Envelope Signals'); xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg) 
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2); 
end
xlim([0, tv(end)]); ylim(ylimite_signal); 
% text(60, 7, 'S1'); text(180, 7, 'S2'); text(300, 7, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist); 
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist); 
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist); 
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: Log Re-scale Stage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(7)
ylimite_signal = [-8, 8];
xlimite_dist = [-7, 7];
X = data_steps{1, 5}.D.X; % Log data
tv = (0:size(X, 1) - 1)/data_container.fs;
durs = data_steps{1, 5}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :);
% x2 = X(1 + dursSum(1):dursSum(2), :);
% x3 = X(1 + dursSum(2):dursSum(3), :);

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
title('Log Envelope Signals'); xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg) 
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2); 
end
xlim([0, tv(end)]); ylim(ylimite_signal);
% text(60, 7, 'S1'); text(180, 7, 'S2'); text(300, 7, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist);
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist);
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist);
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: PCA Stage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(8)
ylimite_signal = [-12, 12];
xlimite_dist = [-7, 7];
X = data_steps{1, 6}.D.X; % PCA data
tv = (0:size(X, 1) - 1)/data_container.fs;
durs = data_steps{1, 6}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :);
% x2 = X(1 + dursSum(1):dursSum(2), :);
% x3 = X(1 + dursSum(2):dursSum(3), :);

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
title('PCA Log Envelope Signals'); xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg)
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2);
end
xlim([0, tv(end)]); ylim(ylimite_signal);
% text(60, 10, 'S1'); text(180, 10, 'S2'); text(300, 10, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist);
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist);
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist);
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: PCA - GRAPH 2 : ONLY FEW PRINCIPAL COMPONENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(9)
ylimite_signal = [-12, 12];
xlimite_dist = [-12, 12];
X = data_steps{1, 6}.D.X; % PCA data
tv = (0:size(X,  1) - 1)/data_container.fs;
durs = data_steps{1, 6}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
npcs = 3;
x1 = X(1:dursSum(1), 1:npcs);
% x2 = X(1 + dursSum(1):dursSum(2), 1:npcs);
% x3 = X(1 + dursSum(2):dursSum(3), 1:npcs);

figure;
subplot(2,1,1); hold on;
plot(tv, X(:, 1:npcs)); 
title('PCA Log Envelope Signals (3PCs)'); xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg)
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2);
end
xlim([0, tv(end)]); ylim(ylimite_signal);
% text(60, 10, 'S1'); text(180, 10, 'S2'); text(300, 10, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist);
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist);
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist);
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: Standard PCA Stage (NOT PERFORMED) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(10)
if transform_options2.standardise_pc==1
ylimite_signal = [-12, 12];
xlimite_dist = [-7, 7];
X = data_steps{1, 7}.D.X; % StandardPCA data
tv = (0:size(X, 1) - 1)/data_container.fs;
durs = data_steps{1, 7}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :);
% x2 = X(1 + dursSum(1):dursSum(2), :);
% x3 = X(1 + dursSum(2):dursSum(3), :);

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
xlabel('Time s'); ylabel('Amplitude uV');
title('Standardized PCA Log Envelope Signals');
for kdur = 1:length(dursSeg); 
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2);
end
xlim([0, tv(end)]); ylim(ylimite_signal);
% text(60, 10, 'S1'); text(180, 10, 'S2'); text(300, 10, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist);
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist);
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist);
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: Downsampling Stage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(11)
ylimite_signal = [-12, 12];
xlimite_dist = [-7, 7];
X = data_steps{1, 8}.D.X; % Downsampled data
tv = (0:size(X, 1) - 1)/fs_new;
durs = data_steps{1, 8}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :);
% x2 = X(1 + dursSum(1):dursSum(2), :);
% x3 = X(1 + dursSum(2):dursSum(3), :);

figure; 
subplot(2,1,1); hold on;
plot(tv, X); 
title('Downsampled PCA Log Envelope Signals');
xlabel('Time s'); ylabel('Amplitude uV');
for kdur = 1:length(dursSeg) 
line([dursSeg(kdur), dursSeg(kdur)], ylimite_signal, 'Color', 'Black', 'LineWidth', 2);
end
xlim([0, tv(end)]); ylim(ylimite_signal);
% text(60, 10, 'S1'); text(180, 10, 'S2'); text(300, 10, 'S3');

subplot(2,3,4); histogram(x1); xlim(xlimite_dist);
title('Sub1');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,5); histogram(x2); xlim(xlimite_dist);
% title('Sub2');  xlabel('Amplitude Values'); ylabel('Freq');
% subplot(2,3,6); histogram(x3); xlim(xlimite_dist);
% title('Sub3');  xlabel('Amplitude Values'); ylabel('Freq');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: COMPARE CHANNELS BETWEEN SUBJECTS FILTERED STAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(12)
% CHANNELS: %16, 45,42, 9,46; Fz, Cz, Pz, C3,C4
chanvec = [16, 45, 42, 9, 46];
chanvecLabels = {'Fz', 'Cz', 'Pz', 'C3','C4'};

X = data_steps{1, 1}.D.X; % Filtered data
durs = data_steps{1, 1}.T;
dursSum = cumsum(durs);
x1 = X(1:dursSum(1), :); 
% x2 = X(1 + dursSum(1):dursSum(2), :); 
% x3 = X(1 + dursSum(2):dursSum(3), :);

figure;
subplot(2,2,1);
kch = 1; plot(x1(:, chanvec(kch)), x1(:, chanvec(kch + 1)), '.');
title('Sub1 vs Sub1 ch filtered'); xlabel('Sub1 ch'); ylabel('Sub1 ch+1');
subplot(2,2,2);
kch = 1; plot(x1(:, chanvec(kch)), x1(:, chanvec(kch + 2)), '.');
title('Sub1 vs Sub1 ch filtered'); xlabel('Sub1 ch'); ylabel('Sub1 ch+2');
subplot(2,2,3);
kch = 1; plot(x1(:, chanvec(kch)), x1(:, chanvec(kch + 3)), '.');
title('Sub1 vs Sub1 ch filtered'); xlabel('Sub1 ch'); ylabel('Sub1 ch+3');
subplot(2,2,4);
kch = 1; plot(x1(:, chanvec(kch)), x1(:, chanvec(kch + 4)), '.');
title('Sub1 vs Sub1 ch filtered'); xlabel('Sub1 ch'); ylabel('Sub1 ch+4');
set(gcf, 'color', 'w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE: COMPARE INTER SUBJECT CHANNELS DOWNSAMPLING STAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if doplot1(13)
chanvec = [1, 5, 10, 15, 18];

X = data_steps{1, 8}.D.X; % Downsampled data
tv = (0:size(X, 1) - 1)/fs_new;
durs = data_steps{1, 8}.T;
dursSum = cumsum(durs);
dursSeg = tv(dursSum);
x1 = X(1:dursSum(1), :); 
% x2 = X(1 + dursSum(1):dursSum(2), :); 
% x3 = X(1 + dursSum(2):dursSum(3), :);

figure;
subplot(2,2,1);
kch = 1; plot(x1(:, chanvec(kch)), x1(:, chanvec(kch + 1)), '.');
title('Sub1 vs Sub1 ch Downsamp'); xlabel('Sub1 ch'); ylabel('Sub1 ch+1');
subplot(2,2,2);
kch = 1; plot(x1(:, chanvec(kch)), x1(:, chanvec(kch + 2)), '.');
title('Sub1 vs Sub1 ch Downsamp'); xlabel('Sub1 ch'); ylabel('Sub1 ch+2');
subplot(2,2,3);
kch = 1; plot(x1(:, chanvec(kch)), x1(:, chanvec(kch + 3)), '.');
title('Sub1 vs Sub1 ch Downsamp'); xlabel('Sub1 ch'); ylabel('Sub1 ch+3');
subplot(2,2,4);
kch = 1; plot(x1(:, chanvec(kch)), x1(:, chanvec(kch + 4)), '.');
title('Sub1 vs Sub1 ch Downsamp'); xlabel('Sub1 ch'); ylabel('Sub1 ch+4');
set(gcf, 'color', 'w');
end

%%

