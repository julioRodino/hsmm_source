% function seqMeasures
function [Occupancy, DurationCat] = seqMeasures2(seqest, nstates )
% Function to get measures from the state path
%   Occupancy percentage:
%   Duration histogram
% Aland Astudillo - AUG 2019

occupancy = zeros(1, nstates);
for kk = 1:max(seqest)
    occupancy(kk) = length(find(seqest==kk));
end
occupancy = occupancy /length(seqest) * 100;

contador = 1;
trozos = [];
cambio = [];
trozos(1) = 1;
for kk = 1:length(seqest) - 1
    if seqest(kk)==seqest(kk + 1)
        trozos(kk + 1) = contador;
    elseif seqest(kk)~=seqest(kk + 1)
        contador = contador + 1;
        trozos(kk + 1) = contador;
        cambio(kk + 1) = 1;
    end
end

catalogo = [];
for cc = 1:contador
    pos1 = find(trozos==cc);
    catalogo(cc, 1) = length(pos1); % size of the state segment in points
    catalogo(cc, 2) = seqest(pos1(1)); % label of the state segment (state index)
end

% rebuilt sequence vector from catalogo
seq2 = [];
punto = 1;
seq2(1) = catalogo(1, 2);
for cc = 1:contador
    posiciones = punto:punto + catalogo(cc, 1) - 1;
    seq2(posiciones) = catalogo(cc, 2); %for check only
    punto = posiciones(end) + 1;
end

% figure,plot(seqest); hold on; plot(seq2,'r'); xlim([0 100]);   

Occupancy = occupancy;
DurationCat = catalogo;
