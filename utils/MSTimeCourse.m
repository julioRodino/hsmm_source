%MICROSTATETIMECOURSE
function [SequencePath, Occupancy, Reseq, Redur] = MSTimeCourse(jmax, nd, ncluster, idx)
% Microstate TIME COURSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
jini = 1;
nj   = size(jmax, 2);
seq = [];
for j = 1:nj - 1
    seq(jini:jmax(j)) = idx(j);
    d = round((jmax(j + 1) - jmax(j))/2);
    seq(jmax(j) + 1:jmax(j) + d) = idx(j);
    jini = jmax(j) + d + 1;
end
seq(jini:nd) = idx(nj);

% DURATION AND FEATURES %%%%%%%%%%%%%%%%%%%%%%%
redur = [];
reseq = [];
dur   = 1;
for j = 2:nd
    if seq(j)==seq(j - 1)
        dur = dur + 1;
        if j==nd
            redur = [redur dur];
            reseq = [reseq seq(j)];
        end
    else
        redur = [redur dur];
        dur   = 1;
        reseq = [reseq seq(j - 1)];
        if j==nd
            redur = [redur 1];
            reseq = [reseq seq(j)];
        end
    end
end

% Occupancy:
for j = 1:ncluster
    occupancy(j) = sum(redur(reseq==j));
end
occupancy = occupancy/nd*100;
% occupancy = [sum(redur(reseq==1)), sum(redur(reseq==2)),...
%              sum(redur(reseq==3)), sum(redur(reseq==4))]/ndata*100;

% %%%%%%%%%%%%%%% Results:
SequencePath = seq;
Occupancy = occupancy;
Redur = redur;
Reseq = reseq;
