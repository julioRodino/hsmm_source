function han = plotMultiChan(da111, fs1,amp,xlimite,ylimite,setname,colore)
% plot multichannel signal, AA 2019

% ss111 = out3.stateseq.cond(1, 1).subj(1, 1).block(1, 1).stateseq;
% xlimite = [0 8]; % ylimite = [0.5 nStates+.5];

tv3 = (0:size(da111,1)-1)/fs1; % time vector for plot

% factor = 1;
% amp = 0.15;
kmat =(1:size(da111,2)); %*factor; 

% figure, % subplot(3,1,1); 
plot(tv3,da111*amp + repmat(kmat,size(da111,1),1),'Color',colore); 
xlim(xlimite); ylim(ylimite); %ylim([-0.5 yHigh]); % yHigh = 5.5
xlabel('Time s'); ylabel('Channel'); title(setname);
set(gcf,'color','w');
han = 1;

